##################################################################
# You can modify CC variable if you have compiler other than GCC
# But the code was designed and tested with GCC
CC = gcc

##################################################################
# Compilation flags
# You should comment the line below for AIX+native cc
CFLAGS = -Wall
CFLAGS += -std=c99

##################################################################
# Libraries
#
# You should comment the line below ('LIBS= -lcrypt')for QNX RTP
# 6.1.0, OpenBSD 2.8 and above, WIN32 (+MinGW)
#LIBS = -lcrypt
LIBM = -lm
# Use lines below for cygwin
# LIBS = 
# LIBM =

##################################################################
# Support for crypted passwords
#
# DO NOT EDIT THE LINE BELOW !!!
CRYPTED_PASS = APG_DONOTUSE_CRYPT
# Coment this if you do not want to use crypted passwords output
# or trying to build programm for win32
CRYPTED_PASS = APG_USE_CRYPT

##################################################################
# Support for ANSI X9.17/SHA1 PRNG
#
# DO NOT EDIT THE LINE BELOW !!!
USE_SHA = APG_USE_SHA
# Coment this if you want to use PRNG X9.17 with SHA-1
USE_SHA = APG_DONOTUSE_SHA

####################################################################
# If you plan to install APG daemon you should look at lines below #
####################################################################

####################################################################
# FreeBSD
#
# Uncoment NOTHING for FreeBSD
#

####################################################################
# Linux
#
# Uncoment line below for LINUX
#CS_LIBS = -lnsl

####################################################################
# Solaris
#
# Uncoment line below for Solaris
#CS_LIBS = -lnsl -lsocket

####################################################################
# QNX RTP 6.1.0
#
# Uncoment line below for QNX RTP 6.1.0
#CS_LIBS = -lsocket

PROGNAME = passutils

SOURCES = \
./src/passutils.c \
./src/apg/bloom.c \
./src/apg/sha/sha.c \
./src/apg/cast/cast.c \
./src/apg/rnd.c \
./src/apg/pronpass.c \
./src/apg/randpass.c \
./src/apg/restrict.c \
./src/apg/errors.c \
./src/apg/apg.c \
./src/apg/getopt.c \
./src/apg/convert.c

OBJECTS = ${SOURCES:.c=.o}

passutils:
	${CC} ${CFLAGS} -D${CRYPTED_PASS} -D${USE_SHA} -o ${PROGNAME} ${SOURCES} ${LIBS} ${LIBM}

strip:
	strip ${PROGNAME}

clean:
	rm -f ${PROGNAME} ${OBJECTS}
