#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "passutils.h"
#include "apg/randpass.h"
#include "apg/pronpass.h"



const int PU_LOWERCASE = S_SL;
const int PU_UPPERCASE = S_CL;
const int PU_NUMBER = S_NB;
const int PU_SPECIAL = S_SS;
const int PU_RESTRICTED_SYMBOL = S_RS;

const char * GenerateRandom(int length, unsigned int charSets) {
    
	char* const password = calloc((length + 10), sizeof(char));
	int result = gen_rand_pass(password, length, length,
                               charSets);
    
	if (result == -1) {
        free(password);
		return NULL;
	}
    
	return password;
}

const char * GeneratePronouceable(int length, unsigned int charSets) {
    
	char* const password = calloc((length + 1), sizeof(char));
	
    /*
	 * Buffer for hypenated word. This is larger than the word itself
	 * as the doc string seems to contradict itself: the returned word
	 * length is inclusive of maxLength, so would be accessing bad memory
	 * if the hypenated buffer was of equal size.
	 */
    char* const hyphenatedWord = calloc((length + 1) * 5, sizeof(char));
    
	int result = gen_pron_pass(password, hyphenatedWord, length, length,
                               charSets);
    free(hyphenatedWord);
    
	if (result == -1) {
        free(password);
		return NULL;
	}
    
	return password;
}

boolean IsLowercase(const char c) {
	return (((int) c) >= 97 && (((int) c) <= 122));
}

boolean IsUppercase(const char c) {
	return (((int) c) >= 65 && (((int) c) <= 90));
}

boolean IsNumber(const char c) {
	return (((int) c) >= 48 && (((int) c) <= 57));
}

boolean IsSpecial(const char c) {
	int ch = (int) c;
	return (ch >= 33 && ch <= 47) || (ch >= 58 && ch <= 64)
    || (ch >= 91 && ch <= 96) || (ch >= 123 && ch <= 126);
}

typedef struct {
	int lowercase;
	int uppercase;
	int number;
	int special;
}  Metrics;

Metrics GetMetrics(const char * const str, const int len) {
	Metrics m = {0, 0, 0, 0};
	for (int i = len - 1; i >= 0; i--) {
		char c = *(str + i);
		if (IsLowercase(c)) {
			m.lowercase += 1;
		} else if (IsUppercase(c)) {
			m.uppercase += 1;
		} else if (IsNumber(c)) {
			m.number += 1;
		} else if (IsSpecial(c)) {
			m.special++;
		}
	}
	return m;
}

float EstimateStrength(const char * const password, const int len) {
    
	Metrics metrics = GetMetrics(password, len);
    
	int alphabetSize = 0;
	alphabetSize += (metrics.lowercase > 0) ? 26 : 0;
	alphabetSize += (metrics.uppercase > 0) ? 26 : 0;
	alphabetSize += (metrics.number > 0) ? 10 : 0;
	alphabetSize += (metrics.special > 0) ? 28 : 0;
    
	float permutations = powf(alphabetSize, len);
	float bits = logf(permutations) / logf(2.0f);
    
	return bits;
}
