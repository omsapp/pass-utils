#ifndef pass_utils_passutils_h
#define pass_utils_passutils_h

#ifdef __cplusplus
extern "C" {
#endif

extern const int PU_UPPERCASE;
extern const int PU_LOWERCASE;
extern const int PU_NUMBER;
extern const int PU_SPECIAL;
extern const int PU_RESTRICTED_SYMBOL;

const char * GenerateRandom(int length, unsigned int charSets);

const char * GeneratePronouceable(int length, unsigned int charSets);

float EstimateStrength(const char * password, const int len);

#ifdef __cplusplus
}
#endif
#endif
